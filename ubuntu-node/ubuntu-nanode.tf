provider "linode" {
  token = var.token
}

resource "linode_instance" "terraform-web" {
        image = var.image
        label = var.label
        group = "Terraform"
        region =  var.region
        type = var.type
        authorized_keys = [ var.auth_keys ]
        root_pass = var.root_pass
}