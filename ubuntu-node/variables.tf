variable "token" {
    type = string
}

variable "image" {
    type = string
    default = "linode/ubuntu20.04"
}

variable "label" {
    type = string
    default = "ubuntu-nanode-uk"
}

variable "region" {
    type = string
    default = "eu-west"
}

variable "type" {
    type = string
    default = "g6-nanode-1"
}

variable "auth_keys" {
    type = string
    default = "~/.ssh/id_rsa_linode.pub"
}

variable "root_pass" {
    type = string
    default = "password"
}

